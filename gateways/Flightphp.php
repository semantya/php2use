<?php
namespace PHP2USE\Gateway\Flightphp;

use Flight;
use PHP2USE\Site;
use PHP2USE\Common as Common;

class Platform extends Common\Platform {
    private $themes = array(
        'default' => 'app/views',
        'admin'   => 'php2use/contrib/admin/files/views',
    );
    public function bootstrap () {
        foreach (Site::$routes as $route) {
            if ($route->is_active()) {
                Flight::route($route->target, $route);
            }
        }
        
        //Flight::set('flight.',       true);
        Flight::set('flight.handle_errors', true);
        Flight::set('flight.log_errors',    true);
        
        $this->set_theme('default');
    }
    
    /**************************************************************************/
    
    public function session_has ($key) {
        if (!session_id()) {
            session_start();
        }
        
        return isset($_SESSION[$key]);
        
        return session_is_registered($key);
    }
    
    public function session_get ($key) {
        if (!session_id()) {
            session_start();
        }
        
        return $_SESSION[$key];
    }
    
    public function session_set ($key, $value) {
        if (!session_id()) {
            session_start();
        }
        
        //session_register($key, $value);
        
        $_SESSION[$key] = $value;
        
        return $this;
    }
    
    public function save () {
        if (!session_id()) {
            session_start();
        }
        
        session_write_close();
    }
    
    /**************************************************************************/
    
    public function redirect ($link) {
        $this->save();
        
        Flight::redirect($link);
    }
    
    /**************************************************************************/
    
    public function set_theme ($key) {
        if (array_key_exists($key, $this->themes)) {
            Flight::set('flight.views.path', $this->themes[$key]);
        }
        
        return $this;
    }
    
    /**************************************************************************/
    
    public function lunch () {
        /*
        Flight::map('error', function(Exception $ex){
            // Handle error
            echo $ex->getTraceAsString();
        });
        
        Flight::map('notFound', function(){
            // Handle not found
        });
        */
        
        $this->save();
        
        Flight::start();
        
        $this->save();
    }
    
    /**************************************************************************/
    
    public function halt ($code, $msg) {
        $this->save();
        
        Flight::halt($code, $msg);
    }
    
    /**************************************************************************/
    
    public function request ()  { return Flight::request(); }
    public function response () { return Flight::response(); }
    
    /**************************************************************************/
    
    public function render_json ($data) { return Flight::json($data); }
    
    public function render_tpl  ($tpl, $args=array(), $layout=true) {
		if ($layout) {
			Flight::render("$tpl.php", $args, 'content');
			
			$this->save();
			
			return Flight::render("layout.php", $args);
		} else {
			return Flight::render("$tpl.php", $args);
		}
    }
}

