<?php

namespace PHP2USE\Common;

ini_set('display_errors', 'On');
/*
ini_set('session.save_handler', 'memcached');
ini_set('session.save_path', 'localhost:11211');
*/

foreach (array(
    'htaccess'         => '.htaccess',
    'composer.json'    => 'composer.json',
    'doctrine-cli.php' => 'cli-config.php',
) as $pth => $trg) {
    if (!file_exists($trg)) {
        shell_exec("cp -afR ".PHP2USE_ROOT."/resources/{$pth} {$trg}");
    }
}

if (!file_exists('vendor/autoload.php')) {
    shell_exec(PHP2USE_ROOT.'/resources/composer.phar install');
    
    shell_exec(PHP2USE_ROOT.'/resources/composer.phar update');
}

require_once('vendor/autoload.php');

foreach (array(
    'core/Helpers',
    'core/Abstract',
    'core/Site',
    'core/HTML5',
    'core/CDN',
    'core/Cache',
    'core/Session',
    'core/SocialAPI',
    
    'helpers/i18n',
    'helpers/mailer',
    'helpers/orm',
    
    'contrib/Araknif',
    'contrib/SSO',
    'contrib/CMS',
    'contrib/Contact',
    'contrib/Startup',
    'contrib/Blog',
    'contrib/BackOffice',
) as $pth) {
    require_once("{$pth}.php");
}

use PHP2USE\Site;
use PHP2USE\Common as Common;

Site::bootstrap();

