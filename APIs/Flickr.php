<?php
namespace PHP2USE\APIs\Flickr;

use PHP2USE\Site;
use PHP2USE\APIs\ReflectionException;
use PHP2USE\APIs\SocialAPI;
use PHP2USE\APIs\SocialResource;
use PHP2USE\Common as Common;

class Bridge extends SocialAPI {
    public function html5_header () {
?>
        
<?php
    }
    public function html5_footer () {
?>
        
<?php
    }
    
    /***************************************************************************************************/
    
    public function opauth_strategy () {
        return array(
            'Flickr' => array(
                'app_id'     => $this->creds['api_key'],
                'app_secret' => $this->creds['api_pki'],
            ),
        );
    }
    
    /***************************************************************************************************/
    
    public function initialize () {
        
    }
    public function connect ($creds, $cfg, $vault) {
        
    }
    
    /***************************************************************************************************/
    
    public function call ($verb, $method, $args=array()) {
        $args['method']         = "flickr.{$method}";
        $args['api_key']        = $this->creds['api_key'];
        $args['format']         = 'json';
        $args['nojsoncallback'] = '1';
        
        $resp = $this->call_json($verb, "https://api.flickr.com/services/rest/?".http_build_query($args));
        
        return $resp;
        if ($resp && $resp->meta) {
            switch (intval($resp->meta->code)) {
                case 200:
                    return $resp;
                    break;
                default:
                    throw new ReflectionException($this, $verb, $method, $args, $resp);
                    break;
            }
        } else {
            print_r($resp); die(1);
        }
        
        return null;
    }
    
    /***************************************************************************************************/
    
    public function user ($uid=null) {
        if ($uid==null) {
            $uid = $this->cfg['user_id'];
        }
        
        $resp = $this->invoke('GET', 'people.getInfo', array('user_id' => $uid));
        
        return $this->reflect('User', $resp, function ($obj) {
            return $obj->person;
        });
    }
    public function getSets ($uid=null) {
        if ($uid==null) {
            $uid = $this->cfg['user_id'];
        }
        
        $resp = $this->invoke('GET', 'photosets.getList', array('user_id' => $uid));
        
        return $this->remap('Photoset', $resp, function ($obj) {
            return $obj->photosets->photoset;
        }, function ($obj) {
            return $obj;
        });
    }
    public function getPhotoSet ($sid) {
        return $this->reflect('Photoset',
            $this->invoke('GET', 'photosets.getPhotos', array(
                'photoset_id' => $sid,
            )),
            function ($obj) {
                return $obj->photoset;
            }
        );
    }
}

class User extends SocialResource {
    public static function narrow($obj) {
        return $obj->nsid;
    }
    
    protected function initialize() {
        
    }
    
    public function uid ()     { return $this->res->id; }
    
    public function photosets() {
        return $this->prn->getSets($this->nrw);
    }
}

class Photoset extends SocialResource {
    public static function narrow($obj) {
        return $obj->id;
    }
    
    private $cover=null;
    
    protected function initialize() {
        $this->cover = null;
        //$this->pics = $this->remap($this->res->photo);
    }
    
    public function uid ()     { return $this->res->id; }
    public function title ()   { return $this->res->title->_content; }
    public function summary () { return $this->res->summary; }
    
    public function cover () {
        if ($this->cover==null) {
            $resp = $this->photos();
            
            if (sizeof($resp)) {
                $this->cover = $resp[0];
            }
        }
        
        return $this->cover;
    }
    
    public function photos () {
        $resp = $this->prn->invoke('GET', 'photosets.getPhotos', array(
            'photoset_id' => $this->uid(),
            'extras'      => implode(',',Photo::$FIELDs),
        ));
        
        // Site::halt(500, print_r($this->uid(), true));
        
        return $this->remap('Photo', $resp, function ($obj) {
            return $obj->photoset->photo;
        });
    }
}

class Photo extends SocialResource {
    public static $FIELDs = array('license','date_upload','date_taken','owner_name','icon_server','original_format','last_update','geo','tags','machine_tags','o_dims','views','media','path_alias','url_sq','url_t','url_s','url_m','url_o');
    
    public static function narrow($obj) {
        return $obj->id;
    }
    
    protected function initialize() {
        
    }
    
    public function uid ()     { return $this->res->id; }
    public function title ()   { return $this->res->title; }
    
    public function link() {
        return "http://farm{$this->res->farm}.staticflickr.com/{$this->res->server}/{$this->res->id}_{$this->res->secret}_b.jpg";
    }
}

