#!/usr/bin/python

import os, sys, couchdb, faker

class Document(object):
    def __init__(self, db, nrw, **data):
        self._db   = db
        self._nrw  = nrw
        self._data = data
    
    database = property(lambda self: self._db)
    narrow   = property(lambda self: self._nrw)
    
    def __getitem__(self, key, default=None):
        return self._data.get(key, default)

class BlogEntry(Document):
    author   = property(lambda self: self['author'])
    when     = property(lambda self: self['when'])
    category = property(lambda self: self['category'])
    
    title    = property(lambda self: self['title'])
    excerpt  = property(lambda self: self['excerpt'])
    content  = property(lambda self: self['content'])

def main(uri, key):
    fake = faker.Factory.create()
    
    conn = couchdb.Server(uri)
    
    if key in conn:
        del conn[key]
    
    db = conn.create(key)
    
    for ind in range(1, 30):
        raw = BlogEntry(db, 'blog',
            author   = fake.name(),
            title    = fake.sentence(),
            excerpt  = fake.text(),
            content  = "\n\n".join(fake.paragraphs()),
            when     = str(fake.date_time()),
            category = fake.word(),
        )
        
        db['blog/%s' % ind] = raw

if __name__=='__main__':
    main(*sys.argv[1:])

