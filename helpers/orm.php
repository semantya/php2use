<?php

namespace PHP2USE\DB;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console;

class ORM {
    private static $cfg = null;
    private static $mgr = null;
    
    /**************************************************************************/
    
    public static function bootstrap () {
        $galaxy = array(
            PHP2USE_ROOT.'/contrib/admin/mapping/',
        );
        
		if (Site::has('orm.mapping')) {
            $pth = PHP2USE_APP.Site::has('orm.mapping');
            
		    if (file_exists($pth)) {
                $galaxy[] = $pth;
            }
        }
        
		if (Site::has('orm.conn')) {
            ORM::$cfg = Setup::createAnnotationMetadataConfiguration($galaxy, Site::get('debug', false));
            
            ORM::$mgr = EntityManager::create(Site::get('orm.conn'), ORM::$cfg);
        }
    }
    
    public static function cmd_helper () {
        ORM::bootstrap();

        return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet(ORM::$mgr);
    }
    
    /**************************************************************************/
    
    public static function mgr () {
        return ORM::$mgr;
    }
    
    public static function repo ($key) {
        return ORM::$mgr->getRepository($key);
    }
    
    public static function find ($key, $nrw) {
        return ORM::$mgr->find($key, $nrw);
    }
    
    public static function query ($dql) {
        return ORM::$mgr->createQuery($dql);
    }
    
    public static function builder () {
        return ORM::$mgr->createQueryBuilder();
    }
    
    public static function persist ($obj) {
        ORM::$mgr->persist($obj);
        ORM::$mgr->flush();
        
        return $obj;
    }
    
    /**************************************************************************/
    
    public static function get_doc ($nrw) {
        try {
            return $sag->get($nrw)->body;
        } catch(SagCouchException $e) {
            //The requested post doesn't exist - oh no!
            if($e->getCode() == "404") {
                $e = new Exception("That post doesn't exist.");
            }
            
            throw $e;
        } catch(SagException $e) {
            //We sent something to Sag that it didn't expect.
            error_log('Programmer error: '.$e->getMessage());
        }
    }
    
    public static function save_doc ($nrw, $doc) {
        try {
            if(!$sag->put($nrw, $doc)->body->ok) {
                error_log('Unable to log a view to CouchDB.');
            }
        } catch(SagCouchException $e) {
            //The requested post doesn't exist - oh no!
            if($e->getCode() == "404") {
                $e = new Exception("That post doesn't exist.");
            }
            
            throw $e;
        } catch(SagException $e) {
            //We sent something to Sag that it didn't expect.
            error_log('Programmer error: '.$e->getMessage());
        }
    }
}

