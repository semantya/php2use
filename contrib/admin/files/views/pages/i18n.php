            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Internationalization
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Verbal</th>
                                            <th>Language</th>
                                            <th>Translation</th>
                                        </tr>
                                    </thead>
                                    <tbody>
<?php foreach ($listing as $trans) { ?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $trans->verb ?></td>
                                            <td class="center"><?php echo $trans->lang ?></td>
                                            <td><?php echo $trans->text ?></td>
                                        </tr>
<?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

