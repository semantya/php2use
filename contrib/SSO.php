<?php
namespace PHP2USE\Contrib\SSO;

use PHP2USE\Site;
use PHP2USE\Common as Common;
use PHP2USE\Contrib\CMS;

/*
require_once( "library/Hybrid/Auth.php" );
 
try{
    
    $hybridauth = new Hybrid_Auth( $config );
    
    $hybridauth_session_data = get_sotred_hybridauth_session( $current_user_id );
     
    $hybridauth->restoreSessionData( $hybridauth_session_data );
     
    $twitter = $hybridauth->getAdapter( "Twitter" );
     
    $user_profile = $twitter->getUserProfile();
} catch( Exception $e ){
    echo "Ooophs, we got an error: " . $e->getMessage();
}
 
function sotre_hybridauth_session( $user_id, $data ){
    $sql = "INSERT INTO users_connections ( user_id, hybridauth_session ) VALUES ( $user_id , $data )";
}
 
function get_sotred_hybridauth_session( $user_id ){
    $sql = "SELECT FROM users_connections WHERE user_id = $user_id ";
}
//*/

class Manager extends Common\Component {
    public static function providers () { return Manager::$prvds; }
    public static function scopes    () { return Manager::$scopes; }
    
    /**************************************************************************/
    
    public static function email () {
        //if (Site::platform()->session_has('nsap.email')) {
        if (isset($_SESSION['nsap.email'])) {
            return Site::platform()->session_get('nsap.email');
        } else {
            return null;
        }
    }
    
    /**************************************************************************/
    
    public static function has_scopes ($targets=array(), $any=false) {
        $usr = Manager::email();
        
        if (!is_array($targets)) {
            $targets = array($targets);
        }
        
        $x=0; $y=0;
        
        foreach (Manager::$scopes as $scope) {
            if (in_array($scope['name'], $targets)) {
                $x += 1;
                
                if ($scope['callback']($usr)) {
                    $y += 1;
                }
            }
        }
        
        if ($any) {
            return (0 < $y);
        } else {
            return ($x == $y);
        }
    }
    
    /**************************************************************************/
    
    private static $hybrid = null;
    private static $scopes = array();
    private static $prvds = array();
    
    public static function bootstrap () {
        /*
        SSO:$hybrid = new Hybrid_Auth(array(
            "base_url"  => Site::get('site.canonical'),
            "providers" => Site::get('sso.hybrids'),
            "debug_mode" => false, 
        ));
        //*/
        
        Manager::$scopes = array(
            array(
                'name'     => 'guest',
                'callback' => function ($user) {
                    return true;
                },
            ),
            array(
                'name'     => 'logged-in',
                'callback' => function ($user) {
                    return Site::platform()->session_has('nsap.email');
                },
            ),
            array(
                'name'     => 'tester',
                'callback' => function ($user) {
                    return isset($_GET['hidden']) or in_array($user, Site::get('acl.testers'));
                },
            ),
            array(
                'name'     => 'staff',
                'callback' => function ($user) {
                    return isset($_GET['technical']) or in_array($user, Site::get('acl.staff'));
                },
            ),
            array(
                'name'     => 'backoffice',
                'callback' => function ($user) {
                    return isset($_GET['sudo']) or in_array($user, Site::get('acl.admins'));
                },
            ),
        );
        
        foreach (Manager::$scopes as $scope) {
            Site::set("acl.{$scope['name']}", array());
        }
		
		//Manager::post_call();
    }
    
    /***************************************************************************************************/
    
    public static function register_routing () {
        Site::map('GET /sso/login',          array('PHP2USE\\Contrib\\SSO\\Manager','view_login_form'), 'admin');
        Site::map('POST /sso/login',         array('PHP2USE\\Contrib\\SSO\\Manager','view_login_post'), 'admin');
        Site::map('/sso/logout',             array('PHP2USE\\Contrib\\SSO\\Manager','view_logout'),     'admin');
        
        Site::map('/sso/auth/(.*)',          array('PHP2USE\\Contrib\\SSO\\Manager','view_federated'),  'admin');
        Site::map('/sso/callback/(.*)',      array('PHP2USE\\Contrib\\SSO\\Manager','view_callback'),   'admin');
        
        Site::map('/sso/debug',              array('PHP2USE\\Contrib\\SSO\\Manager','view_debug'),      'admin');
    }
    
    /***************************************************************************************************/
    
    private static function post_call () {
        SSO\Manager::$prvds = array();
		
        /*
        foreach (Site::apis() as $api) {
            if ($api->supports('oauth')) {
                SSO\Manager::$prvds[] = $api;
            }
        }
        //*/
        
        SSO\Manager::$cfg = array(
	        'security_salt'      => 'Fyw528Drx4dQKJbmwlYf8CFmiiWDx8pM104m',
	        'security_iteration' => 300,
	        'security_timeout'   => '2 minutes',
	        'strategy_dir'       => PHP2USE_ROOT.'/contrib/strategies/',
	        'path'               => '/sso/auth/',
	        'debug'              => true,
	        'callback_url'       => '/sso/callback/',
	        'callback_transport' => 'session',
            'Strategy'           => Site::get('sso.strategy'),
        );
        
        foreach (Site::apis() as $api) {
            if (method_exists($api, 'opauth_strategy')) {
                SSO\Manager::$prvds[] = $api;
                
                SSO\Manager::$cfg['Strategy'] = array_merge(SSO\Manager::$cfg['Strategy'], $api->opauth_strategy());
            }
        }
        

		if (is_array(SSO\Manager::$cfg['Strategy']) and sizeof(SSO\Manager::$cfg['Strategy'])) {
			SSO\Manager::$cfg['strategy'] = SSO\Manager::$cfg['Strategy'];
			
			SSO\Manager::$auth = new Opauth(SSO\Manager::$cfg, false);
		}
    }
    
    /**************************************************************************/
    
    public static function view_login_form () {
        return CMS::current()->render_tpl("sso/login", array(
            'special_page' => 'login',
            'sso_notify' => array(),
        ));
    }
    public static function view_login_post () {
        foreach (Site::get('sso.raw') as $login => $passwd) {
            if ($req->data->email==$login and $req->data->password==$passwd) {
                SSO\Manager::session_set('email', $login);
                SSO\Manager::session_set('payload', array(
                    'identity' => $login,
                    'when'     => new Date(),
                ));
                SSO\Manager::session_set('session', array());
                
                Site::platform()->redirect('/admin/');
            }
        }
        
        return CMS::current()->render_tpl("sso/login", array(
            'special_page' => 'login',
            'sso_notify' => array(
                array('level' => 'danger', 'text' => "Email or password incorrect !"),
            ),
        ));
    }
    public static function view_logout () {
        foreach (array('opauth','nsap.email','nsap.payload') as $key) {
            unset($_SESSION[$key]);
            
            Site::platform()->session_set($key, null);
            
            //session_unregister($key);
        }
        
        Hybrid_Provider_Adapter::logout();
        
        Site::platform()->redirect('/');
    }
    
    /**************************************************************************/
    
    private static function session_get ($key, $default=null) {
        return Site::platform()->session_get("nsap.{$key}", $default);
    }
    
    private static function session_set ($key, $value) {
        Site::platform()->session_set("nsap.{$key}", $value);
        
        return $value;
    }
    
    /**************************************************************************/
    
    public static function view_federated ($prvd) {
        $req = Site::platform()->request();
        
        if (array_key_exists($prvd, Site::get('sso.hybrids'))) {
            $adapter = SSO\Manager::$hybrid->authenticate($prvd);
            
            SSO\Manager::view_callback($prvd);
        }
        
        Site::platform()->redirect($req->query['next'] or '/');
    }
    
    /**************************************************************************/
    
    public static function view_callback ($prvd) {
        if (array_key_exists($prvd, Site::get('sso.hybrids'))) {
            $adapter = SSO\Manager::$hybrid->authenticate($prvd);
            
            $payload   = $adapter->getUserProfile();
            
            $sess_data = SSO\Manager::$hybrid->getSessionData();
            
            SSO\Manager::session_set('email',   $sess_data);
            SSO\Manager::session_set('payload', $payload);
            SSO\Manager::session_set('session', $sess_data);
        }
        
        Site::platform()->redirect($req->query['next'] or '/');
    }
    
    /**************************************************************************/
    
    public static function view_debug () {
        return CMS\Manager::current()->render_tpl("sso/debug", array(
            'special_page' => 'debug',
        ));
    }
}

