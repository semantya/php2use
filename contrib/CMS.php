<?php
namespace PHP2USE\Contrib\CMS;

use PHP2USE\Site;
use PHP2USE\Common as Common;

class CMS_Page {
    private $pattern;
    public $title;
    private $assets;
    
    public function CMS_Page ($pattern) {
        $this->pattern = $pattern;
        $this->assets  = array();
        
        $fqdn = Flight::request()->url;
        
        $this->title   = Cache::get("cms://{$pattern}#title", ucfirst(Site::get('site.title')));
    }
    
    /**************************************************************************/
    
    public function bootstrap () {
        
    }
    
    /**************************************************************************/
    
    private function register_asset ($kind, $link, $args=array()) {
        if (!array_key_exists($kind, $this->assets)) {
            $this->assets[$kind] = array();
        }
        
        $this->assets[$kind][] = array($link, $args);
    }
    
    /**************************************************************************/
    
    public function render_tpl ($tpl, $args=array()) {
        return Site::platform()->render_tpl("{$tpl}", $args);
    }
    public function render_page ($tpl, $args=array()) {
        return $this->render_tpl("pages/{$tpl}", $args);
    }
    
    /**************************************************************************/
    
    public function cdn () { return HTML5::dyntag(); }
}

class Manager extends Common\Component {
    public static function bootstrap () {
        
    }
    
    /***************************************************************************************************/
    
    public static function register_routing () {
        Site::map('/@slug.html', array('PHP2USE\\Contrib\\CMS\\Manager','view_page'));
        
        Site::map('/',           array('PHP2USE\\Contrib\\CMS\\Manager','homepage'));
    }
    
    /**************************************************************************/
    
    private static $curr = null;
    public static function current() {
        if (Manager::$curr==null) {
            $ptn = Site::platform()->request()->url;
            
            Manager::$curr = Manager::locate($ptn);
        }
        
        return Manager::$curr;
    }
    public static function locate($pattern) {
        $resp = new CMS_Page($pattern);
        
        return $resp;
    }
    
    /**************************************************************************/
    
    public static function homepage () {
        Site::render_tpl("home", array(
            'is_homepage' => true,
        ));
    }
    
    public static function view_page ($slug) {
        if (file_exists(PHP2USE_ROOT."/../app/views/pages/{$slug}.php")) {
            Site::render_page($slug);
        } else {
            Site::render_tpl('error/not-found');
        }
    }
}

