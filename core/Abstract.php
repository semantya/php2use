<?php
namespace PHP2USE\Common;

class SocialException extends \Exception {
    private $api = null;
    
    public function __construct ($api, $args = array()) {
        $this->api = $api;
        
        call_user_func_array(array($this, 'initialize'), $args);
    }
}

class Helper {
    protected function extract ($obj, $cb=null) {
        $resp = null;
        
        if ($cb!=null and $obj!=null) {
            try {
                $resp = $cb($obj);
            } catch (Exception $ex) {
                Site::handle_exc('Api', 'Flickr', $ex, array(
                    'resp' => $resp,
                ));
            }
        } else {
            $resp = $obj;
        }
        
        return $resp;
    }
    protected function wrap ($cls, $obj, $cb=null) {
        $ns = $this->rewrite_ns($cls);
        
        $obj = $this->extract($obj, $cb);
		
		if (is_object($obj) and $obj!=null) {
			$nrw = call_user_func(array($ns, 'narrow'), $obj);
			
			eval('$resp = new $ns($this, $nrw, $obj);');
			
			return $resp;
		} else {
			return null;
		}
    }
    protected function remap ($cls, $resp, $mpp=null, $cb=null) {
        $coll = array();
        
        if ($resp!=null) {
            $lst = $resp;
            
            if ($mpp!=null) {
                $lst = $mpp($resp);
            }
            
            foreach ($lst as $obj) {
                $coll[] = $this->wrap($cls, $obj, $cb);
            }
        }
        
        return $coll;
    }
}

class Component {
}

class HashTable extends Helper {
    
}

class Module {
    public static function set ($key, $value) {
        Site::$cfg[$key] = $value;
    }
    public static function get ($key, $default=null) {
        if (Site::has($key)) {
            return Site::$cfg[$key];
        } else {
            return $default;
        }
    }
    public static function has ($key) {
        return array_key_exists($key, Site::$cfg);
    }
}

class Platform {
    
}

