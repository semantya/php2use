<?php
namespace PHP2USE;

use Faker\Factory as FakeFactory;
use PHP2USE\Contrib\SSO;

class WebRoute {
    private $fqdn;
    public  $target;
    public  $callback;
    private $theme;
    private $scopes;
    
    public function __construct ($target, $callback, $theme, $fqdn, $scopes, $any) {
        $this->fqdn     = $fqdn;
        $this->target   = $target;
        $this->callback = $callback;
        $this->theme    = $theme;
        $this->scopes   = $scopes;
        $this->any      = $any;
    }
    
    public function is_active () {
        $req = Site::platform()->request();
        
        if ($this->fqdn!=null and $this->fqdn!=$req->base) {
            return false;
        }
        
        if (sizeof($this->scopes) and !(SSO\Manager::has_scopes($this->scopes, $this->any))) {
            return false;
        }
        
        return true;
    }
    
    public function __invoke() {
        $params = func_get_args();
        
        Site::platform()->set_theme($this->theme);
        
        return call_user_func_array($this->callback, $params);
    }
}

class Site extends Common\Module {
    public static $cfg = array();
    private static $apis = array();
    private static $root = null;
    private static $platform = null;
    public static $routes = array();
    
    private static $excs = array();
    
    /***************************************************************************************************/
    
    public static function handle_exc ($cmp, $mod, $exc, $params=array()) {
        Site::$excs[] = array(
            'component'  => $exc,
            'module'     => $exc,
            'exception'  => $exc,
            'parameters' => $params,
        );
    }
    
    /***************************************************************************************************/
    
    public static function html5_header () {
?>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="HandheldFriendly" content="True">
    <meta name="apple-touch-fullscreen" content="yes" />
    <meta name="MobileOptimized" content="320">
    <link rel="profile" href="https://gmpg.org/xfn/11.html">

    <link rel="shortcut icon" href="/assets/ico/favicon.html">

    <title><?php echo Site::get('site.title') ?></title>

    <link rel="canonical" href="<?php echo Site::get('site.canonical') ?>" />
    <link rel="publisher" href="<?php echo Site::get('site.publisher') ?>"/>

<!--
    <meta name="author" content="FIFOTHEMES.COM">
    <meta name="description" content="Agency Parallax Responsive HTML5/CSS3 Template from FIFOTHEMES.COM">
-->
<?php
        Site::trigger('html5_header');
        Site::diffuse('html5_header');
    }
    public static function html5_footer () {
        Site::trigger('html5_footer');
        Site::diffuse('html5_footer');
?>
    </body>
</html>
<?php
    }
    
    /***************************************************************************************************/
    
    private static function load ($path) {
        require_once(Site::$root.'/'.$path);
        
        return null;
    }
    public static function load_core ($section, $path) {
        require_once(PHP2USE_ROOT."/{$section}/{$path}.php");
    }
    private static function load_app ($section, $path) {
        Site::load("app/{$section}/{$path}.php");
    }
    
    public static function app_inc ($path) {
        Site::load_app('inc', $path);
    }
    public static function app_view ($path) {
        Site::load_app('views', $path);
    }
    
    /***************************************************************************************************/
    
    private $modules=array();
    
    private static function trigger ($event, $args=array(), $targets=null) {
        if ($targets==null) {
            $targets = array();
            
            foreach (Site::get('core.ext.modules', array('Cache','Session')) as $key) {
                $targets[] = "PHP2USE\{$key}\Manager";
            }
            
            foreach (Site::get('core.ext.contrib', array('BackOffice','CMS','ContactForm','SSO')) as $key) {
                $targets[] = "PHP2USE\Contrib\{$key}\Manager";
            }
        }
        
        foreach ($targets as $cls) {
            if (method_exists($cls, $event)) {
                forward_static_call_array(array($cls, $event), $args);
            }
        }
    }
    private static function diffuse ($event, $args=array()) {
        $lst = array_merge(array(
            Contrib\CMS\Manager::current(),
        ), array_values(Site::$apis));
        
        foreach ($lst as $cls) {
            if (method_exists($cls, $event)) {
                call_user_func_array(array($cls, $event), $args);
            }
        }
    }
    
    public static function bootstrap ($os='flightphp', $root=null) {
        if ($root==null) {
            $root = getcwd();
        }
        
        if (Site::$root==null) {
            Site::$root = $root;
        }
        
        if (Site::$root!=null) {
            Site::load_core('gateways', ucfirst($os));
            
            $cls = "PHP2USE\\Gateway\\".ucfirst($os)."\\Platform";
            
            Site::$platform = new $cls();
            
            Site::trigger('bootstrap');
            Site::load('app/configure.php');
            Site::diffuse('bootstrap');
            
            Site::trigger('initialize');
            Site::load('app/connect.php');
            Site::diffuse('initialize');
            
            //Site::trigger('register_routing');
            
            Site::trigger('register_routing');
            Site::load('app/routing.php');
            Site::diffuse('register_routing');
            
            Site::$platform->bootstrap();
            
            Site::trigger('bootstrap', array(), array('ORM','i18n','Mailer'));
        }
    }
    
    public static function lunch () {
        if (Site::$root!=null) {
            Site::$platform->lunch();
        }
    }
    
    /***************************************************************************************************/
    
    public static function map ($target, $callback, $theme='default', $fqdn=null, $scopes=array('guest'), $any=false) {
        Site::$routes[] = new WebRoute($target, $callback, $theme, $fqdn, $scopes, $any);
    }
    
    /***************************************************************************************************/
    
    private static function render ($path, $args=array()) {
        return Site::app_view($path);
    }
    
    /***************************************************************************************************/
    
    public static function OS ($key, $default='') {
        return getenv($key) or $default;
    }
    
    /***************************************************************************************************/
    
    public static function platform () {
        return Site::$platform;
    }
    
    public static function halt ($code, $msg) {
        return Site::$platform->halt($code, $msg);
    }
    
    public static function render_json ($data) {
        return Site::$platform->render_json($data);
    }
    
    public static function render_tpl ($tpl, $args=array(), $layout=true) {
        return Site::$platform->render_tpl($tpl, $args, $layout);
    }
    
    public static function render_page ($tpl, $args=array(), $layout=true) {
        return Site::render_tpl("pages/{$tpl}", $args, $layout);
    }
    
    /***************************************************************************************************/
    
    public static function connect ($key, $creds, $cfg=array(), $vault=array()) {
        $key = strtolower($key);
        $pth = ucfirst($key);
        $nrw = "PHP2USE\\APIs\\{$pth}\\Bridge";
        
        Site::load_core('APIs', $pth);
        
        if (!array_key_exists($key, Site::$apis)) {
            $cnx = null;
            
            if (true or array_keys_exists(array('api_key','api_pki'), $cfg)) {
                $cnx = new $nrw($key, $creds, $cfg, $vault);
                
                Site::$apis[$key] = $cnx;
                
                Site::$cfg[$key] = $cnx;
            }
        }
        
        return Site::$apis[$key];
    }
    
    public static function api ($key) {
        $key = strtolower($key);
        
        if (array_key_exists($key, Site::$apis)) {
            return Site::$apis[$key];
        } else {
            return Site::connect($key);
        }
    }
    
    public static function apis () {
        return array_values(Site::$apis);
    }
    
    /***************************************************************************************************/
    
    public static function set ($key, $value) {
        Site::$cfg[$key] = $value;
		
		switch ($key) {
			case 'site.domain':
				Site::set('site.canonical', "http://{$value}/");
				break;
		}
    }
    public static function get ($key, $default=null) {
        if (Site::has($key)) {
            return Site::$cfg[$key];
        } else {
            return $default;
        }
    }
    public static function has ($key) {
        return array_key_exists($key, Site::$cfg);
    }
    
    /***************************************************************************************************/
    
    private static $faker = null;
    
    public static function fake () {
        if (Site::$faker==null) {
            Site::$faker = FakeFactory::create();
        }
        
        return Site::$faker;
    }
}

