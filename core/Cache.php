<?php
namespace PHP2USE\Cache;

use PHP2USE\Site;
use PHP2USE\Common as Common;

class Manager extends Common\Module {
    private static $conn = null;
    private static $mapping = array(
        'memcache' => array(
            'class'    => 'Memcached',
            'defaults' => array(array('127.0.0.1', 11211)),
        ),
        'redis' => array(
            'class'    => 'Predis\\Client',
            'defaults' => "tcp://127.0.0.1:6379",
        ),
    );
    
    public static function bootstrap () {
        
    }
    
    public static function connect () {
        Manager::cursor();
    }
    
    /***************************************************************************************************/
    
    protected static function connect_memcache ($cls, $drv, $cfg) {
        $conn = new $cls();
        
        $conn->addServers($cfg);
        
        return $conn;
    }
    
    protected static function connect_redis ($cls, $drv, $cfg) {
        $conn = new $cls($cfg);
        
        return $conn;
    }
    
    protected static function cursor() {
        if (Manager::$conn==null) {
            foreach (array('memcache','redis') as $key) {
                $driver = Manager::$mapping[$key];
                
                if (Manager::$conn==null) {
                    $driver['name'] = $key;
                    $driver['callback'] = array('PHP2USE\\Cache\\Manager', "connect_{$key}");
                    
                    $driver['config'] = Site::get("cache.{$key}", Manager::$mapping[$key]['defaults']);
                    
                    $cls = Manager::$mapping[$key]['class'];
                    
                    if (class_exists($cls)) {
                        try {
                            $cnx = call_user_func($driver['callback'], $cls, $driver, $driver['config']);
                        } catch (Exception $ex) {
                            print_r($ex); die();
                            
                            $cnx = null;
                        }
                    } else {
                        $cnx = null;
                    }
                    
                    if ($cnx!=null) {
                        Manager::$conn = new DummyDriver();
                    }
                }
            }
        }
        
        if (Manager::$conn==null) {
            Manager::$conn = new DummyDriver();
        }
        
        return Manager::$conn;
    }
    
    /***************************************************************************************************/
    
    public static function set ($key, $value) {
        Manager::cursor()->set($key, $value);
        
        return $value;
    }
    public static function get ($key, $default=null) {
        if (Manager::has($key)) {
            return Manager::cursor()->get($key);
        } else {
            return Manager::set($key, $default);
        }
    }
    public static function has ($key) {
        $value = Manager::cursor()->get($key);
        
        if (!($value)) {
            return false;
            /*
            if (Manager::cursor()->getResultCode()==Memcached::RES_NOTFOUND) {
                return false;
            } else {
                // log error
                // ...
                return false;
            }
            //*/
        } else {
            return true;
        }
    }
    
    /***************************************************************************************************/
    
    public static function keys () {
        return Manager::cursor()->getAllKeys();
    }
    
    public static function flush () {
        Manager::cursor()->flush();
    }
}

class Wrapper extends Common\HashTable {
    protected $format;
    protected $defaults;
    protected $prefix;
    
    public function __construct ($format, $defaults=array(), $prefix=array()) {
        $this->format   = $format;
        $this->defaults = $defaults;
        $this->prefix   = $prefix;
    }
    
    protected function format ($narrow) {
        return call_user_func_array('sprintf', array_merge(array($this->format), $this->prefix, $narrow));
    }
    
    public function has ($narrow) {
        return Manager::has($this->format($narrow));
    }
    
    public function get ($narrow, $default=null) {
        return Manager::get($this->format($narrow), $default);
    }
    
    public function set ($narrow, $value) {
        return Manager::set($this->format($narrow), $value);
    }
    
    public function del ($narrow) {
        return Manager::del($this->format($narrow));
    }
}

class DummyDriver {
    protected $buffer;
    
    public function __construct ($defaults=array()) {
        $this->buffer = $defaults or array();
    }
    
    public function has ($narrow) {
        return array_key_exists($narrow, $this->buffer);
    }
    
    public function get ($narrow, $default=null) {
        if ($this->has($narrow)) {
            return $this->buffer[$narrow];
        } else {
            return $default;
        }
    }
    
    public function set ($narrow, $value) {
        $this->buffer[$narrow] = $value;
        
        return $value;
    }
    
    public function del ($narrow) {
        array_remove($narrow, $this->buffer);
        
        return true;
    }
}

