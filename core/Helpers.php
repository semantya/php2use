<?php

namespace PHP2USE;

function array_keys_exists ($keys, $coll, $any=false) {
    $cmp = 0;
    
    foreach ($keys as $key) {
        if (array_key_exists($key, $coll)) {
            $cmp += 1;
        }
    }
    
    if ($any==true) {
        return (0 < $cmp);
    } else {
        return ($cmp==sizeof($coll));
    }
}

